#Specify the AWS provider and region
provider "aws" {
  region = "us-east-1" # Change to your preferred region
}

# Create an S3 bucket
resource "aws_s3_bucket" "example" {
  bucket = "my-unique-bucket-vinoth" # Change to a unique bucket name

  acl    = "private" # Optional: Set the ACL for the bucket

  tags = {
    Name        = "My bucket"
    Environment = "Dev"
  }
}